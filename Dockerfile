FROM python:3.6

WORKDIR /src

COPY ./requirements.txt .

RUN pip3 install -r requirements.txt

COPY . .

RUN export FLASK_APP=app.py

#RUN export POSTGRESQL_URL=postgresql://worker:worker@localhost/app

RUN export POSTGRESQL_URL=postgresql://worker:worker@db:5432/app

EXPOSE 5000

#CMD ["flask", "run", "--host", "0.0.0.0", "db upgrade"]

ENTRYPOINT ["./entrypoint.sh"]